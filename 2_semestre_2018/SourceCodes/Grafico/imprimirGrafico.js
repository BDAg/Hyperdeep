function imprimirGrafico(string, json) {
    var com = document.getElementById(string);
    var x = Object.keys(json[0]['Valores_Bandas']);
    var y = [];
    var count = 1;
    json.forEach(j => {
        var y2 = [];
        for (var e in j['Valores_Bandas']) {
            y2.push(j['Valores_Bandas'][e]);
        }
        y.push({
            label: 'Analise ' + count,
            data: y2,
            borderColor: getRandomColor(),
            fill: false,
        });
        count++;
    });
    var grafico = new Chart(com, {
        type: 'line',
        data: {
            labels: x,
            datasets: y,
        }, options: {
            title: {
                display: true,
                text: 'Analise',
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Reflectancia'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Comprimeto de onda (nm)'
                    }
                }],
            }
        }
    });
}
// gerar cores aleatórias
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}