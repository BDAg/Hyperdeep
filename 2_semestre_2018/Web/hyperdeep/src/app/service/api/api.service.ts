import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../model/user';
import { Router } from '@angular/router';
import { CustomerService } from '../customer/customer.service';

// const api_url:string = 'http://10.64.245.148:8080/';
const api_url:string = 'https://hyperdeep.herokuapp.com/';

@Injectable({
  providedIn: 'root'
})

export class ApiService { 

  constructor(
    private http: HttpClient,
    private router: Router,
    private customer: CustomerService
  ) { }

  makeRequest(url, content, method) {
    const token = this.customer.getToken();
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': `bearer ${token}` }),
      withCredentials: true
    };

    return method === 'post' 
      ? this.http.post<User>(`${api_url}${url}`, content, httpOptions)
      : this.http.get<User>(`${api_url}${url}`, httpOptions)
  }  

  login(email: string, password: string): Observable<User> {
    return this.makeRequest(`sessao/login`, {
      email: email,
      password: password
    }, 'post');
  }

  cadastro(email: string, senha: string, nome: string): Observable<User> {
    return this.makeRequest(`sessao/cadastro`, {
      email: email,
      password: senha,
      nome: nome
    }, 'post');
  }

  esqueciSenha(email: string): Observable<User> {
    return this.makeRequest(`sessao/esqueciSenha`, {
      email: email
    }, 'post');
  }

  /* requires auth */
  logout(): Observable<User> {
    return this.makeRequest(`sessao/logout`, {  }, 'post');
  }

  entradaDados(CSVInput: string, filename: string): Observable<User> {
    return this.makeRequest(`analise/entradaDados`, { 
      CSVInput: CSVInput, 
      filename: filename
    }, 'post');
  }

  visualizarAnalises(): Observable<User> {
    return this.makeRequest(`analise/visualizarAnalises`, {  }, 'get');
  }

}