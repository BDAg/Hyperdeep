import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../model/user';
import { ApiService } from '../../service/api/api.service';
import { CustomerService } from '../../service/customer/customer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: User = new User();
  constructor(private api: ApiService,
    private customer: CustomerService,
    private router: Router) { }

  ngOnInit() {
    this.customer.setToken(null);
  }

  toCadastro() {
    this.router.navigateByUrl('/cadastro');
  }

  login() {
    this.api.login(
      this.model.email,
      this.model.password
    )
      .subscribe(
        user => {
          if (user.token) {
            console.log(user.token);
            this.customer.setToken(user.token);
            this.router.navigateByUrl('');
          }
        },
        err => {
          console.error(err.error);
        });
  }

  esqueciSenha() {
    this.api.esqueciSenha(
      this.model.email
    )
      .subscribe(
        user => {
          console.log('envio confirmado');
        },
        err => {
          console.error(err.error);
        });
  }

}
