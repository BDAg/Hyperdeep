import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../model/user';
import { ApiService } from '../../service/api/api.service';
import { CustomerService } from '../../service/customer/customer.service';

@Component({
  selector: 'app-analise',
  templateUrl: './analise.component.html',
  styleUrls: ['./analise.component.css']
})
export class AnaliseComponent implements OnInit {
  model: User = new User();  
  filename: any;
  file: any;
  /*scrollPos: any;*/
  analises: any;
  charts: any;

  constructor(
  	private router: Router, 
    private api: ApiService,
    private customer: CustomerService,    
  ){}
  
  logout() {
    this.api.logout(
    )
      .subscribe(
        response => {
          this.customer.removeToken();
          this.router.navigate([(<any>'/login')]);
        },
        err => {
          console.error(err.error);
        });    
  }

  fileDragHover(event) {
    event.stopPropagation();
    event.preventDefault();
    event.type == 'dragover' 
      ? document.getElementById(`idAnaliseUpload`).classList.add('dragover')
      : document.getElementById(`idAnaliseUpload`).classList.remove('dragover')
  }

  refreshFile(event): void {
    const ev = event.type === 'change'
      ? event.target.files
      : event.dataTransfer.files

    const fileReader = new FileReader();    
    fileReader.onload = (content => {
      this.file = fileReader.result;
      this.filename = ev[0].name;
      document.getElementById('idDragSpan').classList.add('picked');
      document.getElementById('idUploadButton').classList.add('picked');
    });
    fileReader.readAsText(ev[0]);
  }

  fileSelectHandler(event) {
    this.fileDragHover(event);
    this.refreshFile(event);
  }

  newAnalysis(): void {
    this.api.entradaDados(
      this.file,
      this.filename
    )
      .subscribe(
        response => {
          this.analises = response;
        },
        err => {
          console.error(err.error);
        });
  }

  refreshAnalyses() {
    this.api.visualizarAnalises()
      .subscribe(
        response => {
          this.analises = response;
        },
        err => {
          console.error(err.error);
        });
  }  

  groupPieData(talhaoName) {
    return this.analises[talhaoName].reduce((result, data) => {
      const parsedN = parseFloat(data.resultado);
      let index = undefined;

      if (0 <= parsedN && parsedN <= 10) { index = 0 } 
      else if (11 <= parsedN && parsedN <= 20) { index = 1 }
      else if (21 <= parsedN && parsedN <= 30) { index = 2 }
      else if (31 <= parsedN) { index = 3 }
      result[index][1] += 1;
      return result;
    }, [
      ['0 ~ 10', 0],
      ['11 ~ 20', 0],
      ['21 ~ 30', 0],
      ['31+', 0]
    ]);
  }

  culturaLineData(talhaoName) {
    let talhaoData = this.analises[talhaoName].reduce((result, analise) => {
      analise.cultura in result
        ? result[analise.cultura].push(analise.resultado)
        : result[analise.cultura] = [analise.resultado]
      return result;
    }, {});

    return Object.entries(talhaoData).reduce((result, entry:{}) => {
      const avrg = entry[1].reduce((prev, curr) => parseFloat(prev) + parseFloat(curr), 0) / entry[1].length;
      result.push([entry[0], avrg]);
      return result;
    }, [])
  }  

  toggleAnalysis(talhaoName): void {
    const allAnalyses = Array.from(document.getElementById(`idAnaliseContainer`).children);

    if (document.getElementById(`talhao_${talhaoName}`).classList.contains('selected')) {
      /*document.getElementById(`talhao_${talhaoName}`).addEventListener('transitionend', (talhaoName) => {
      document.getElementById(`idAnaliseContainer`).scroll({ top: this.scrollPos, behavior: 'smooth' });
      document.getElementById(`talhao_${talhaoName}`).removeEventListener('transitionend', () => {});
      });*/
      document.getElementById(`talhao_${talhaoName}`).classList.remove('selected');
      document.getElementById(`idAnaliseContainer`).classList.remove('expanded');
      document.getElementById(`idAnaliseUpload`).classList.remove('expanded');
      allAnalyses.forEach(analysis => analysis.classList.remove('expanded'));
    } else {
      //this.scrollPos = document.getElementById(`idAnaliseContainer`).scrollTop;*/
      document.getElementById(`talhao_${talhaoName}`).classList.add('selected');
      document.getElementById(`idAnaliseContainer`).classList.add('expanded');
      document.getElementById(`idAnaliseUpload`).classList.add('expanded');
      allAnalyses.forEach(analysis => analysis.classList.add('expanded'));

      const lineData = this.analises[talhaoName].map(analise => [null, parseFloat(analise.resultado)] );
      const pieData = this.groupPieData(talhaoName);
      const culturaLineData = this.culturaLineData(talhaoName);

      this.charts = [{
        title: 'Análises agrupadas por similaridade',
        type: 'PieChart',
        columnNames: ['Analise', 'AnalisesIguais'],
        options: {
          width: '45%',
          height: '100%',
          backgroundColor: { fill: 'transparent'},
          colors: ['#167F39']
        },
        roles: [],
        data: pieData
      },
      {
        title: 'Valores de nitrogênio no talhão',
        type: 'LineChart',
        columnNames: ['Analise', 'Nitrogenio'],
        options: {
          width: '45%',
          height: '100%',
          backgroundColor: { fill: 'transparent'},
          colors: ['#167F39']
        },
        roles: [],
        data: lineData
      },
      {
        title: 'Nitrogênio médio por cultura',
        type: 'BarChart',
        columnNames: ['Analise', 'Nitrogenio'],
        options: {
          width: '45%',
          height: '100%',
          backgroundColor: { fill: 'transparent'},
          colors: ['#167F39']
        },
        roles: [],
        data: culturaLineData
      }
      ];
    }
  }

  ngOnInit() {
    this.filename = 'Clique aqui para escolher um arquivo';

    window.addEventListener('dragover', (e) => { e.preventDefault(); }, false);
    window.addEventListener('drop', (e) => { e.preventDefault(); }, false);

    document.getElementById('idAnaliseUpload').addEventListener('dragover', this.fileDragHover, false);
    document.getElementById('idAnaliseUpload').addEventListener('dragleave', this.fileDragHover, false);
    document.getElementById('idAnaliseUpload').addEventListener('drop', this.fileSelectHandler.bind(this), false);

    this.refreshAnalyses();
  }
}