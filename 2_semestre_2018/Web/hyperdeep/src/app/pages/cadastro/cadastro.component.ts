import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../../model/user';
import { ApiService } from '../../service/api/api.service';
import { CustomerService } from '../../service/customer/customer.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
  model: User = new User();
  confirmar_password: string;
  constructor(private api: ApiService,
    private customer: CustomerService,
    private router: Router) { }

  ngOnInit() { }

  cadastro() {
    if (this.model.password == this.confirmar_password) {
      this.api.cadastro(
        this.model.email,
        this.model.password,
        this.model.nome
      )
        .subscribe(
          user => {
              this.router.navigateByUrl('/login');
          },
          err => {
            console.error(err.error);
          });
    }else{
      alert('As senhas são diferentes!');
    }
  }

}
