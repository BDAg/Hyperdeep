import { Routes, RouterModule } from '@angular/router';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { AnaliseComponent } from './pages/analise/analise.component';
import { ModuleWithProviders } from '../../node_modules/@angular/compiler/src/core';
import { LoginComponent } from './pages/login/login.component';

import { AuthGuard } from './guard/auth/auth.guard';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'cadastro', component: CadastroComponent },
    { path: '', component: AnaliseComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES, {useHash: true});