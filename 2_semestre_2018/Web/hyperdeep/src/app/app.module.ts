import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { AnaliseComponent } from './pages/analise/analise.component';
import { routing } from './app.routing';
import { LoginComponent } from './pages/login/login.component';

import { GoogleChartsModule } from 'angular-google-charts';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AuthGuard } from './guard/auth/auth.guard';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CadastroComponent,
    AnaliseComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    routing,
    GoogleChartsModule,
    HttpClientModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
