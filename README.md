﻿# HyperDeep




Projeto iniciado na Fatec Shunji Nishimura em Pompéia-SP com o propósito de criar um novo metódo de analise foliar por meio de imagens hiperspectrais.



# Técnicas abordadas no momento !!!

  

- Utilização de imagens espectrais (novo metódo de análise foliar).
 
- Utilização de "aprendizado de maquina" e outras técnicas.



# Nivel atual do projeto

- [x] Inicio do Projeto Integrador 1º Semestre 2019

- [x] Definições do Projeto (Documentação)

- [x] Definições da Solução (Documentação)

- [x] Sprint 1 (Desenvolvimento)
- [x] Sprint 2 (Desenvolvimento)
 
- [x] Sprint 3 (Desenvolvimento)
- [x] Sprint 4 (Desenvolvimento)

- [ ] Finalização (Encerramento do Projeto)



### Ferramentas e Tecnologias utilizadas




**Para saber como instalar as ferramentas clique** [aqui](https://gitlab.com/BDAg/Hyperdeep/wikis/Ambiente-de-desenvolvimento)

* *Python*

* *JavaScript*

* *NodeJS*

* *MongoDB*

* *MySQL*

* *Visual Studio Code*

* *Linux/Windows*


### Desenvolvedores do projeto



Equipe formada por alunos do curso de Big Data no Agronegócio e seus respectivos perfis no LinkedIn.



| Equipe | Perfil no LinkedIn  |
| ------ | ------ |
| Carlos Benedetti | https://www.linkedin.com/in/carlos-eduardo-b-9349b8138/ |
| Gustavo de Barros (CTO) | https://www.linkedin.com/in/gustavo-barros-000b8815b/ |
| Leonardo Viana | https://www.linkedin.com/in/leonardo-viana-3436a1155/ |
| Matheus Sena | https://www.linkedin.com/in/matheus-sena-85073916b/ |
| Rafael Petraca (CEO) | https://www.linkedin.com/in/rafael-petraca-7ba70b150/ |
| Tales Gava | https://www.linkedin.com/in/tales-gava-marchetti-25554316a/ |



# Contato

- Email: hyper.deep@hotmail.com

**Projeto OpenSource!**



