const model_ = {
    list_inputs: []
};
var didTrain = false

const train = async function () {
    document.getElementById('predit-apliable').classList = ''
    var btnTrain = document.getElementById("btnTrain");

    console.log('training...');
    btnTrain.innerHTML = "treinando..."
    btnTrain.setAttribute('disabled','disabled')
    btnTrain.style.background = "#ffff0059;";

    function addExample(div, cls) {

        var imgs = document.getElementById(div).getElementsByTagName("img");

        for (var i = 0; i < imgs.length; i++) {
            var img = imgs[i];
            model_['classifier'].addExample(
                model_['mobilenetModule'].infer(
                    tf.browser.fromPixels(img),
                    'false')
                , cls
            );
        }
    }

    model_['classifier'] = knnClassifier.create();
    model_['mobilenetModule'] = await mobilenet.load();

    for (let i = 0; i < model_['list_inputs'].length; i++) {
        const input = model_['list_inputs'][i];
        addExample(input['div'], input['class']);
    }

    console.log('Ok');
    document.getElementById('predit-apliable').classList = 'full'
    btnTrain.style.background = "green";
    btnTrain.removeAttribute('disabled')
    btnTrain.innerHTML = '<i class="fa fa-graduation-cap" aria-hidden="true"></i>treinar'
    showToast()
}

const predict = async function () {
    console.log('Predictions...');

    var divs_test = document.getElementById("folhaPred").getElementsByTagName("div");
    for (var i = 0; i < divs_test.length; i++) {
            var div = divs_test[i];
            var img = div.getElementsByTagName("img")[0];
            var p = document.createElement("p");1

            const x = tf.browser.fromPixels(
                img
            );
            const xlogits = model_['mobilenetModule'].infer(x, 'conv_preds');
            var results = await model_['classifier'].predictClass(xlogits);

            console.log(results['confidences']);
            p.innerText = results['label'];
            div.appendChild(p);
    }
}

const loadImage = function (idDiv, idFile) {

    var div = document.getElementById(idDiv);
    div.innerHTML = "";
    var files = document.getElementById(idFile).files;

    for (let i = 0; i < files.length; i++) {
        const file = files[i];

        new Promise((resolve) => {
            var reader = new FileReader();

            reader.onloadend = function () {
                var img = document.createElement("img");
                img.src = reader.result;
                img.style.maxHeight = 400
                img.style.maxWidth = 400
                div.appendChild(img);
            }

            reader.readAsDataURL(file);

            resolve(true);
        });
    }
}

const loadTest = function () {

    var folhaPred = document.getElementById("folhaPred");
    folhaPred.innerHTML = "";
    var files = document.getElementById("file").files;

    for (let i = 0; i < files.length; i++) {
        const file = files[i];

        new Promise((resolve) => {
            var reader = new FileReader();

            reader.onloadend = function () {
                var img = document.createElement("img");
                var div = document.createElement("div");

                img.src = reader.result;

                div.appendChild(img);
                folhaPred.appendChild(div);
                
            }

            reader.readAsDataURL(file);

            resolve(true);
        });
    }
}

const addClass = async function () {
    var inputs = document.getElementById('inputs');
    var input_name = document.getElementById('input_name').value;

    model_['list_inputs'].push({
        div: `div_${input_name}`,
        class: input_name
    });

    var div = document.createElement("div");
    div.classList.add("item");
    div.innerHTML =

        `
        <div class="label-input">
        <input type="file" id="${input_name}" class="inputfile" multiple onchange="loadImage('div_${input_name}','${input_name}')">
         <label for="${input_name}"><i class="fa fa-file-image-o" aria-hidden="true" style="padding-right:2vh:"> </i>
         
         Adicionar Imagem: <span style="color:white">${input_name}</span></label>   
         </div>
    <div id="div_${input_name}"></div>`;
          

    inputs.appendChild(div);
}
function showToast() {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
  
    // Add the "show" class to DIV
    x.className = "show";
  
    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }

