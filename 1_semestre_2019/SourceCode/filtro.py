import cv2
import os
import numpy as np

def main():
    entries = os.listdir('in/')
    for imgName in entries:
        doBin(imgName)
        doEro(imgName)
def doBin(imgName):
    img = cv2.imread("in/"+imgName)
    grayscaled = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)


    retval2,threshold2 = cv2.threshold(grayscaled,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    # cv2.imshow("img",threshold2)
    # cv2.waitKey(0)
    cv2.imwrite("bin/"+imgName,threshold2)
    print("bin/"+imgName)
    cv2.destroyAllWindows()
def doEro(imgName):
    img = cv2.imread("in/"+imgName)
    kernel = np.ones((5,5),np.uint8)
    img_Erod = cv2.erode(img,kernel,iterations=1)

    cv2.imwrite("ero/"+imgName,img_Erod)
if _name_ == "_main_":
    main()